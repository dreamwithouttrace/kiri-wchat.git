<?php


namespace wchat\qq;



/**
 * Class Subject
 * @package wchat\qq
 */
class Subject extends \wchat\base\Subject
{

	/**
	 * @return string
	 */
	public function getUrl(): string
	{
		return '/api/json/subscribe/SendSubscriptionMessage';
	}


	/**
	 * @return string
	 */
	public function getHost(): string
	{
		return 'api.q.qq.com';
	}

}
