<?php


namespace wchat\qq\result;

/**
 * Class RedHatResult
 * @package qq\result
 */
class RedHatResult
{

    public mixed $result;
    public mixed $res_info;

    public mixed $listid;
    public mixed $state;
    public mixed $total_num;
    public mixed $recv_num;
    public mixed $total_amount;
    public mixed $recv_amount;
    public mixed $recv_details;
    public mixed $uin;


    const int RED_HAT_STATUS_PAID     = 1;
    const int RED_HAT_STATUS_SNATCHED = 2;
    const int RED_HAT_STATUS_EXPIRED  = 3;
    const int RED_HAT_STATUS_REFUNDED = 4;


    /**
     * RedHatResult constructor.
     * @param array $array
     */
    public function __construct(array $array)
    {
        $this->init($array);
    }

    /**
     * @param array $array
     * @return void
     */
    private function init(array $array): void
    {
        foreach ($array as $key => $value) {
            if (!property_exists($this, $key)) {
                continue;
            }
            $this->{$key} = $value;
        }
    }


    /**
     * @param string $field
     * @return mixed
     */
    public function get(string $field): mixed
    {
        return $this->$field;
    }


    /**
     * @return bool
     * 是否已完成支付
     */
    public function isPaid(): bool
    {
        return $this->state == self::RED_HAT_STATUS_PAID;
    }

    /**
     * @return bool
     * 是否已抢完
     */
    public function isSnatched(): bool
    {
        return $this->state == self::RED_HAT_STATUS_SNATCHED;
    }


    /**
     * @return bool
     */
    public function isExpired(): bool
    {
        return $this->state == self::RED_HAT_STATUS_EXPIRED;
    }


    /**
     * @return bool
     */
    public function isRefunded(): bool
    {
        return $this->state == self::RED_HAT_STATUS_REFUNDED;
    }

}
