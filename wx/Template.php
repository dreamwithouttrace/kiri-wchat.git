<?php
/**
 * Created by PhpStorm.
 * User: whwyy
 * Date: 2018/4/8 0008
 * Time: 9:49
 */

namespace wchat\wx;

/**
 * Class Template
 * @package wchat\wx
 */
class Template extends \wchat\base\Template
{


	/**
	 * @return string
	 */
	public function getUrl(): string
	{
		return '/cgi-bin/message/wxopen/template/send';
	}


	/**
	 * @return string
	 */
	public function getHost(): string
	{
		return 'api.weixin.qq.com';
	}

}
