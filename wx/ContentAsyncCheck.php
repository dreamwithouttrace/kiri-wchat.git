<?php


namespace wchat\wx;


/**
 * Class ContentAsyncCheck
 * @package wchat
 */
class ContentAsyncCheck
{
	private mixed $_ToUserName = '';
	private mixed $_FromUserName = '';
	private mixed $_CreateTime = '';
	private mixed $_MsgType = '';
	private mixed $_Event = '';
	private mixed $_isrisky = '';
	private mixed $_extra_info_json = '';
	private mixed $_appid = '';
	private mixed $_trace_id = '';
	private mixed $_status_code = '';

	/**
	 * @return string
	 */
	public function getToUserName(): mixed
	{
		return $this->_ToUserName;
	}

	/**
	 * @return string
	 */
	public function getFromUserName(): mixed
	{
		return $this->_FromUserName;
	}

	/**
	 * @return string
	 */
	public function getCreateTime(): mixed
	{
		return $this->_CreateTime;
	}

	/**
	 * @return string
	 */
	public function getMsgType(): mixed
	{
		return $this->_MsgType;
	}

	/**
	 * @return string
	 */
	public function getEvent(): mixed
	{
		return $this->_Event;
	}

	/**
	 * @return string
	 */
	public function getIsrisky(): mixed
	{
		return $this->_isrisky;
	}

	/**
	 * @return string
	 */
	public function getExtraInfoJson(): mixed
	{
		return $this->_extra_info_json;
	}

	/**
	 * @return string
	 */
	public function getAppid(): mixed
	{
		return $this->_appid;
	}

	/**
	 * @return string
	 */
	public function getTraceId(): mixed
	{
		return $this->_trace_id;
	}

	/**
	 * @return string
	 */
	public function getStatusCode(): mixed
	{
		return $this->_status_code;
	}

	/**
	 * @param array $params
	 * @return ContentAsyncCheck
	 */
	public static function instance(array $params): static
	{
		static $class = null;
		if ($class === null) {
			$class = new ContentAsyncCheck();
		}
		return $class->init($params);
	}

    /**
     * @param array $params
     * @return $this
     */
	private function init(array $params): static
	{
		foreach ($params as $item => $param) {
			$this->{'_' . $item} = $param;
		}
		return $this;
	}

	/**
	 * @return bool
	 */
	public function isRisky(): bool
	{
		return intval($this->_isrisky) === 0;
	}

	/**
	 * @return bool
	 */
	public function isSuccess(): bool
	{
		return $this->_status_code === 0;
	}

}
