<?php


namespace wchat\wx;

use wchat\common\Help;

class Notify extends SmallProgram
{
	public mixed $appid = '';
	public mixed $mch_id = '';
	public mixed $device_info = '';
	public mixed $nonce_str = '';
	public mixed $sign = '';
	public mixed $sign_type = '';
	public mixed $result_code = '';
	public mixed $err_code = '';
	public mixed $err_code_des = '';
	public mixed $openid = '';
	public mixed $is_subscribe = '';
	public mixed $trade_type = '';
	public mixed $bank_type = '';
	public mixed $total_fee = '';
	public mixed $settlement_total_fee = '';
	public mixed $fee_type = '';
	public mixed $cash_fee = '';
	public mixed $cash_fee_type = '';
	public mixed $coupon_fee = '';
	public mixed $coupon_count = '';
	public mixed $coupon_type_n = '';
	public mixed $coupon_id_n = '';
	public mixed $coupon_fee_n = '';
	public mixed $transaction_id = '';
	public mixed $out_trade_no = '';
	public mixed $attach = '';
	public mixed $time_end = '';
	public mixed $return_code = '';

	/**
	 * @return bool
	 * 判断是否完成支付
	 */
	public function isSuccess(): bool
	{
		if ($this->getReturnCode() != "SUCCESS") {
			return false;
		}

		if ($this->getResultCode() != 'SUCCESS') {
			return false;
		}

		return true;
	}

	/**
	 * @param array $params
	 * @return $this
	 */
	public function setPayNotifyData(array $params): static
	{
		if (!$this->validation($params)) {
			$this->setResultCode('FAIL');
			$this->setErrCodeDes('签名错误');
			unset($params['result_code'], $params['err_code_des']);
		}
		foreach ($params as $key => $val) {
			$this->__set($key, $val);
		}
		return $this;
	}


    /**
     * @param string $name
     * @param mixed $value
     * @return void
     */
	public function __set(string $name, mixed $value): void
	{
		if (property_exists($this, $name)) {
			$this->$name = $value;
		}
	}


	/**
	 * @param array $params
	 * @return bool
	 */
	public function validation(array $params): bool
	{
		$sign = $params['sign'];
		unset($params['sign']);

		$signType = $this->payConfig->getSignType();
		$privateKey = $this->payConfig->pay->wx->secret;
		$nowSign = Help::sign($params, $privateKey, $signType);
		if ($sign === $nowSign) {
			return true;
		}
		return false;
	}

	/**
	 * @return mixed|string
	 */
	public function getAppid(): mixed
	{
		return $this->appid;
	}

	/**
	 * @param mixed|string $appid
	 */
	public function setAppid(mixed $appid): void
	{
		$this->appid = $appid;
	}

	/**
	 * @return mixed|string
	 */
	public function getMchId(): mixed
	{
		return $this->mch_id;
	}

	/**
	 * @param mixed|string $mch_id
	 */
	public function setMchId(mixed $mch_id): void
	{
		$this->mch_id = $mch_id;
	}

	/**
	 * @return mixed|string
	 */
	public function getDeviceInfo(): mixed
	{
		return $this->device_info;
	}

	/**
	 * @param mixed|string $device_info
	 */
	public function setDeviceInfo(mixed $device_info): void
	{
		$this->device_info = $device_info;
	}

	/**
	 * @return mixed|string
	 */
	public function getNonceStr(): mixed
	{
		return $this->nonce_str;
	}

	/**
	 * @param mixed|string $nonce_str
	 */
	public function setNonceStr(mixed $nonce_str): void
	{
		$this->nonce_str = $nonce_str;
	}

	/**
	 * @return mixed|string
	 */
	public function getSign(): mixed
	{
		return $this->sign;
	}

	/**
	 * @param mixed|string $sign
	 */
	public function setSign(mixed $sign): void
	{
		$this->sign = $sign;
	}

	/**
	 * @return mixed|string
	 */
	public function getSignType(): mixed
	{
		return $this->sign_type;
	}

	/**
	 * @param mixed|string $sign_type
	 */
	public function setSignType(mixed $sign_type): void
	{
		$this->sign_type = $sign_type;
	}

	/**
	 * @return mixed|string
	 */
	public function getResultCode(): mixed
	{
		return $this->result_code;
	}

	/**
	 * @param mixed|string $result_code
	 */
	public function setResultCode(mixed $result_code): void
	{
		$this->result_code = $result_code;
	}

	/**
	 * @return mixed|string
	 */
	public function getErrCode(): mixed
	{
		return $this->err_code;
	}

	/**
	 * @param mixed|string $err_code
	 */
	public function setErrCode(mixed $err_code): void
	{
		$this->err_code = $err_code;
	}

	/**
	 * @return mixed|string
	 */
	public function getErrCodeDes(): mixed
	{
		return $this->err_code_des;
	}

	/**
	 * @param mixed|string $err_code_des
	 */
	public function setErrCodeDes(mixed $err_code_des): void
	{
		$this->err_code_des = $err_code_des;
	}

	/**
	 * @return mixed|string
	 */
	public function getOpenid(): mixed
	{
		return $this->openid;
	}

	/**
	 * @param mixed|string $openid
	 */
	public function setOpenid(mixed $openid): void
	{
		$this->openid = $openid;
	}

	/**
	 * @return mixed|string
	 */
	public function getIsSubscribe(): mixed
	{
		return $this->is_subscribe;
	}

	/**
	 * @param mixed|string $is_subscribe
	 */
	public function setIsSubscribe(mixed $is_subscribe): void
	{
		$this->is_subscribe = $is_subscribe;
	}

	/**
	 * @return mixed|string
	 */
	public function getTradeType(): mixed
	{
		return $this->trade_type;
	}

	/**
	 * @param mixed|string $trade_type
	 */
	public function setTradeType(mixed $trade_type): void
	{
		$this->trade_type = $trade_type;
	}

	/**
	 * @return mixed|string
	 */
	public function getBankType(): mixed
	{
		return $this->bank_type;
	}

	/**
	 * @param mixed|string $bank_type
	 */
	public function setBankType(mixed $bank_type): void
	{
		$this->bank_type = $bank_type;
	}

	/**
	 * @return mixed|string
	 */
	public function getTotalFee(): mixed
	{
		return $this->total_fee;
	}

	/**
	 * @param mixed|string $total_fee
	 */
	public function setTotalFee(mixed $total_fee): void
	{
		$this->total_fee = $total_fee;
	}

	/**
	 * @return mixed|string
	 */
	public function getSettlementTotalFee(): mixed
	{
		return $this->settlement_total_fee;
	}

	/**
	 * @param mixed|string $settlement_total_fee
	 */
	public function setSettlementTotalFee(mixed $settlement_total_fee): void
	{
		$this->settlement_total_fee = $settlement_total_fee;
	}

	/**
	 * @return mixed|string
	 */
	public function getFeeType(): mixed
	{
		return $this->fee_type;
	}

	/**
	 * @param mixed|string $fee_type
	 */
	public function setFeeType(mixed $fee_type): void
	{
		$this->fee_type = $fee_type;
	}

	/**
	 * @return mixed|string
	 */
	public function getCashFee(): mixed
	{
		return $this->cash_fee;
	}

	/**
	 * @param mixed|string $cash_fee
	 */
	public function setCashFee(mixed $cash_fee): void
	{
		$this->cash_fee = $cash_fee;
	}

	/**
	 * @return mixed|string
	 */
	public function getCashFeeType(): mixed
	{
		return $this->cash_fee_type;
	}

	/**
	 * @param mixed|string $cash_fee_type
	 */
	public function setCashFeeType(mixed $cash_fee_type): void
	{
		$this->cash_fee_type = $cash_fee_type;
	}

	/**
	 * @return mixed|string
	 */
	public function getCouponFee(): mixed
	{
		return $this->coupon_fee;
	}

	/**
	 * @param mixed|string $coupon_fee
	 */
	public function setCouponFee(mixed $coupon_fee): void
	{
		$this->coupon_fee = $coupon_fee;
	}

	/**
	 * @return mixed|string
	 */
	public function getCouponCount(): mixed
	{
		return $this->coupon_count;
	}

	/**
	 * @param mixed|string $coupon_count
	 */
	public function setCouponCount(mixed $coupon_count): void
	{
		$this->coupon_count = $coupon_count;
	}

	/**
	 * @return mixed|string
	 */
	public function getCouponTypeN(): mixed
	{
		return $this->coupon_type_n;
	}

	/**
	 * @param mixed|string $coupon_type_n
	 */
	public function setCouponTypeN(mixed $coupon_type_n): void
	{
		$this->coupon_type_n = $coupon_type_n;
	}

	/**
	 * @return mixed|string
	 */
	public function getCouponIdN(): mixed
	{
		return $this->coupon_id_n;
	}

	/**
	 * @param mixed|string $coupon_id_n
	 */
	public function setCouponIdN(mixed $coupon_id_n): void
	{
		$this->coupon_id_n = $coupon_id_n;
	}

	/**
	 * @return mixed|string
	 */
	public function getCouponFeeN(): mixed
	{
		return $this->coupon_fee_n;
	}

	/**
	 * @param mixed|string $coupon_fee_n
	 */
	public function setCouponFeeN(mixed $coupon_fee_n): void
	{
		$this->coupon_fee_n = $coupon_fee_n;
	}

	/**
	 * @return mixed|string
	 */
	public function getTransactionId(): mixed
	{
		return $this->transaction_id;
	}

	/**
	 * @param mixed|string $transaction_id
	 */
	public function setTransactionId(mixed $transaction_id): void
	{
		$this->transaction_id = $transaction_id;
	}

	/**
	 * @return mixed|string
	 */
	public function getOutTradeNo(): mixed
	{
		return $this->out_trade_no;
	}

	/**
	 * @param mixed|string $out_trade_no
	 */
	public function setOutTradeNo(mixed $out_trade_no): void
	{
		$this->out_trade_no = $out_trade_no;
	}

	/**
	 * @return mixed|string
	 */
	public function getAttach(): mixed
	{
		return $this->attach;
	}

	/**
	 * @param mixed|string $attach
	 */
	public function setAttach(mixed $attach): void
	{
		$this->attach = $attach;
	}

	/**
	 * @return mixed|string
	 */
	public function getTimeEnd(): mixed
	{
		return $this->time_end;
	}

	/**
	 * @param mixed|string $time_end
	 */
	public function setTimeEnd(mixed $time_end): void
	{
		$this->time_end = $time_end;
	}

	/**
	 * @return mixed|string
	 */
	public function getReturnCode(): mixed
	{
		return $this->return_code;
	}

	/**
	 * @param mixed|string $return_code
	 */
	public function setReturnCode(mixed $return_code): void
	{
		$this->return_code = $return_code;
	}

}
