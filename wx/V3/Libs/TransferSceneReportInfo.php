<?php

namespace wchat\wx\V3\Libs;

use Arrayable;

readonly class TransferSceneReportInfo implements Arrayable
{


    public function __construct(
        public string $info_type,
        public string $info_content,
    )
    {
    }


    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            "info_type"    => $this->info_type,
            "info_content" => $this->info_content
        ];
    }


}
