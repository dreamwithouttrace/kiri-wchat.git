<?php

namespace wchat\wx\V3\Libs;

use Exception;

class TransferDetail
{


    private string $out_bill_no;
    private string $transfer_scene_id;
    private string $openid;
    private string $user_name            = '';
    private int    $transfer_amount;
    private string $transfer_remark;
    private string $notify_url           = '';
    private string $user_recv_perception = '';
    private array  $transfer_scene_report_infos;


    /**
     * @return string
     */
    public function getOutBillNo(): string
    {
        return $this->out_bill_no;
    }


    /**
     * @param string $out_bill_no
     * @return void
     */
    public function setOutBillNo(string $out_bill_no): void
    {
        $this->out_bill_no = $out_bill_no;
    }


    /**
     * @return string
     */
    public function getTransferSceneId(): string
    {
        return $this->transfer_scene_id;
    }


    /**
     * @param string $transfer_scene_id
     * @return void
     */
    public function setTransferSceneId(string $transfer_scene_id): void
    {
        $this->transfer_scene_id = $transfer_scene_id;
    }


    /**
     * @return string
     */
    public function getOpenid(): string
    {
        return $this->openid;
    }


    /**
     * @param string $openid
     * @return void
     */
    public function setOpenid(string $openid): void
    {
        $this->openid = $openid;
    }


    /**
     * @return string
     */
    public function getUserName(): string
    {
        return $this->user_name;
    }


    /**
     * @param string $user_name
     * @return void
     */
    public function setUserName(string $user_name): void
    {
        $this->user_name = $user_name;
    }


    /**
     * @return int
     */
    public function getTransferAmount(): int
    {
        return $this->transfer_amount;
    }


    /**
     * @param int $transfer_amount
     * @return void
     */
    public function setTransferAmount(int $transfer_amount): void
    {
        $this->transfer_amount = $transfer_amount;
    }


    /**
     * @return string
     */
    public function getTransferRemark(): string
    {
        return $this->transfer_remark;
    }


    /**
     * @param string $transfer_remark
     * @return void
     */
    public function setTransferRemark(string $transfer_remark): void
    {
        $this->transfer_remark = $transfer_remark;
    }


    /**
     * @return string
     */
    public function getNotifyUrl(): string
    {
        return $this->notify_url;
    }


    /**
     * @param string $notify_url
     * @return void
     */
    public function setNotifyUrl(string $notify_url): void
    {
        $this->notify_url = $notify_url;
    }


    /**
     * @return string
     */
    public function getUserRecvPerception(): string
    {
        return $this->user_recv_perception;
    }


    /**
     * @param string $user_recv_perception
     * @return void
     */
    public function setUserRecvPerception(string $user_recv_perception): void
    {
        $this->user_recv_perception = $user_recv_perception;
    }


    /**
     * @return array<TransferSceneReportInfo>
     */
    public function getTransferSceneReportInfos(): array
    {
        return $this->transfer_scene_report_infos;
    }


    /**
     * @param array $transfer_scene_report_infos
     * @return void
     */
    public function setTransferSceneReportInfos(TransferSceneReportInfo ...$transfer_scene_report_infos): void
    {
        $this->transfer_scene_report_infos = $transfer_scene_report_infos;
    }


    /**
     * @return array
     * @throws Exception
     */
    public function toArray(): array
    {
        $transfer_scene_report_infos = [];
        foreach ($this->transfer_scene_report_infos as $transfer_scene_report_info) {
            $transfer_scene_report_infos[] = $transfer_scene_report_info->toArray();
        }
        $array = [
            'out_bill_no'                 => $this->out_bill_no,
            'transfer_scene_id'           => $this->transfer_scene_id,
            'openid'                      => $this->openid,
            'transfer_amount'             => $this->transfer_amount,
            'transfer_remark'             => $this->transfer_remark,
            'transfer_scene_report_infos' => $transfer_scene_report_infos,
        ];
        foreach ($array as $key => $value) {
            if (empty($value)) {
                throw new  Exception('必填项' . $key . '不能为空.');
            }
        }
        if (!empty($this->user_name)) {
            $array['user_name'] = $this->user_name;
        }
        if (!empty($this->notify_url)) {
            $array['notify_url'] = $this->notify_url;
        }
        if (!empty($this->user_recv_perception)) {
            $array['user_recv_perception'] = $this->user_recv_perception;
        }
        return $array;
    }

}
