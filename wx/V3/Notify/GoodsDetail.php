<?php

namespace wchat\wx\V3\Notify;

class GoodsDetail
{
    public string $goods_remark    = "商品备注信息";
    public int    $quantity        = 1;
    public int    $discount_amount = 1;
    public string $goods_id        = "M1006";
    public int    $unit_price      = 100;

    /**
     * @param array $value
     */
    public function __construct(readonly public array $value)
    {
        $this->goods_remark    = $this->value['goods_remark'];
        $this->quantity        = $this->value['quantity'];
        $this->discount_amount = $this->value['discount_amount'];
        $this->goods_id        = $this->value['goods_id'];
        $this->unit_price      = $this->value['unit_price'];
    }
}